import slowclap as sc
import speech_recognition as sr
from msvcrt import getch
import pygame 
import os
import pyttsx3
import win32com.client
import time
import datetime
import locale
import requests
from googletrans import Translator
import feedparser
import json
from threading import Thread

#--------------------------------------------Class-----------------------------------------------

class minuteur(Thread):

    def __init__(self,hr,min,sec):
        Thread.__init__(self)
        self.hr = int(hr)
        self.min = int(min)
        self.sec = int(sec)
        self.end = 0
        self.totalSec = (self.hr * 3600 + self.min * 60 + self.sec)
        pygame.mixer.init()
        pygame.mixer.music.load("./media/alarm.wav")

    def run(self):
        while self.end != self.totalSec:
            time.sleep(1)
            self.end += 1
        pygame.mixer.music.play(0)
        while pygame.mixer.music.get_busy():
            key = ord(getch())
            if key == 32:
                pygame.mixer.music.stop()
                break
            else:
                continue

class alarmClock(Thread):

    def __init__(self):
        self.now = datetime.datetime.now()
        self.dayNb = self.now.strftime("%w")
        self.hr = 0
        self.min = 0
        self.isSpeaking = 0
        self.dayAlarms = []
        self.ttsEngine = pyttsx3.init(driverName="sapi5")
        pygame.mixer.init()
        Thread.__init__(self)
        print("reveil lancé")
        with open('alarmClock.json', 'r') as f:
            self.alarmFile = json.load(f)

    def getAlarmOfDay(self):
        del self.dayAlarms[:]
        for alarm in self.alarmFile:
            if alarm['dayOfWeek'] == int(self.dayNb):
                self.dayAlarms.append([alarm["hr"],alarm["min"],alarm["name"],0]) #the 0 is a value to check if it as already ringed
        print(self.dayAlarms)

    def run(self):
        self.getAlarmOfDay()
        self.now = datetime.datetime.now()
        self.dayNb = self.now.strftime("%w")
        while True:
            self.now = datetime.datetime.now()
            for alarm in self.dayAlarms:
                if alarm[0] == self.now.hour and alarm[1] == self.now.minute and alarm[3] == 0:
                    print(alarm[2])
                    pygame.mixer.music.load("./media/alarm.wav")
                    pygame.mixer.music.play(0)
                    while pygame.mixer.music.get_busy():
                        key = ord(getch())
                        if key == 27:
                            pygame.mixer.music.stop()
                            alarm[3] = 1 #set the value to 1 for stop ring again for the same alarm
                            self.isSpeaking = 1
                            break
                else:
                    continue
            if self.dayNb != self.now.strftime("%w"):
                self.dayNb = self.now.strftime("%w")
                self.getAlarmOfDay()
#----------------------------------------Global Variable-----------------------------------------

tr = Translator()
city = "trelaze"
apiKey = "0d9411d3266d65a98a69336d25646090"
userName = "Chris"

locale.setlocale(locale.LC_ALL, 'fr_FR')
engine = pyttsx3.init(driverName="sapi5")

recogniser = sr.Recognizer()
feed = sc.MicrophoneFeed()
mic = sr.Microphone()
mic.list_microphone_names()
detector = sc.AmplitudeDetector(feed, threshold=222200)
nbClap = 0
ttsFileNb = 0

#-----------------------------------------Functions----------------------------------------------
def tts(textToSay):
    engine.say(textToSay)
    engine.runAndWait()


def commandeToText(txtWithCommand,command):
    commandPos = txtWithCommand.index(command)
    textToReturn = txtWithCommand[commandPos + len(command):]
    return textToReturn

def readNews():
    now = datetime.datetime.now()
    day = now.day
    month = now.month
    year = now.year
    with open('rss.json', 'r') as f:
        rssFile = json.load(f)     
    for feed in rssFile:
        titre = []
        d = feedparser.parse(feed["url"])
        freshNews = 0
        for news in d['entries']:
            if news['published_parsed'][1] == month and news['published_parsed'][2] == day and news['published_parsed'][0] == year:
                titre.append(tr.translate( news['title'], dest='fr').text)
                freshNews += 1
        if freshNews == 0:
            tts("pas d'acctualités sur " + feed["name"])
        else:
            tts(str(freshNews) + ": acctualités sur " + feed["name"])
            for titrefr in titre:
                tts(titrefr)

def readNotes():
    numLines = len(open("notes.txt").readlines())
    print("nbLigne in notes : "+ str(numLines))
    note = open("notes.txt")
    if numLines == 0:
        tts("Pardon je n'ai rien de noté pour aujourd'hui.")
    else:  
        tts("Vous devez :")
        for i in range(0,numLines):
            tts(str(note.readline()))

def sayTime():
    now = datetime.datetime.now()
    tts("nous sommes le "+now.strftime("%A")+" "+str(now.strftime("%d"))+" "+now.strftime("%B")+" "+str(now.strftime("%Y")))
    tts("il est "+str(now.hour)+" heure "+str(now.minute))

def sayWeather():
    response = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+city+",fr&appid="+apiKey)
    data = response.json()
    tts("il fait "+str(round(data['main']['temp'] - 273.15,2)) + "degré et le temps est : "+ tr.translate(data['weather'][0]['description'], dest='fr').text)

def wakeUp():
    tts("Bonjour " + userName)
    sayTime()
    sayWeather()
    readNotes()
    readNews() 
#-------------------------------------------init-------------------------------------------------
myAlarmClock = alarmClock()
myAlarmClock.start() 

#-------------------------------------------MAIN-------------------------------------------------

for clap in detector:
    while myAlarmClock.isSpeaking == 1:
        wakeUp()
        myAlarmClock.isSpeaking = 0
        continue
    nbClap += 1
    print(nbClap)
    #detection du calp
    if nbClap >= 3:
        nbClap = 0
        print("ok")
        with mic as source:
            #Synthese vocal post detection
            recogniser.adjust_for_ambient_noise(source)

            tts("oui ?")

            audio=recogniser.listen(source)
        try:
            recoText=recogniser.recognize_google(audio,language = "fr")
        except :
            recoText="rien entendu"
            #Synthese vocal post detection Quand rien detecté
            
            tts("Pardon je n'est rien entendu.")
        print(recoText)
        
        if recoText == "rien entendu":
            pass
        else:
            #Ajouté un rapelle
            if recoText.lower().find("rappelle-moi") !=-1 or recoText.lower().find("fais-moi penser") !=-1:
                note = open("notes.txt","a")
                if recoText.find("rappelle-moi") !=-1:
                    textToNote = str(commandeToText(recoText,"rappelle-moi"))
                if recoText.find("fais-moi penser") !=-1:
                    textToNote = str(commandeToText(recoText,"fais-moi penser"))
                if textToNote.find("de",0,4) != -1:
                    textToNote = textToNote.replace("de ","",1)
                if textToNote.find("d'",0,4) != -1:
                    textToNote = textToNote.replace("d' ","",1)
                if textToNote.find("à",0,4) != -1:
                    textToNote = textToNote.replace("à ","",1)

                note.write(textToNote +"\n")
                note.close() 
                tts("c'est noté.")
            #Stop    
            elif recoText.lower().find("non") !=-1 or recoText.lower().find("ta gueule") !=-1 or recoText.lower().find("stop") !=-1:
                tts("ok je marrette")
            #Bonjour
            elif recoText.lower().find("bonjour") !=-1 or recoText.lower().find("salut") !=-1:
                tts("Bonjour maitre, je reste à votre service")
            #Lire la liste
            elif recoText.lower().find("que dois-je faire") !=-1 or recoText.lower().find("qu'est-ce que je dois faire") !=-1:
                readNotes() 
            #get hour
            elif recoText.lower().find("quelle heure est-il") !=-1 or recoText.lower().find("il est quelle heure") !=-1 or recoText.lower().find("dis-moi l'heure") != -1:
                now = datetime.datetime.now()
                tts("il est "+str(now.hour)+" heure "+str(now.minute))
            #get Date
            elif recoText.lower().find("quel jour sommes-nous") !=-1 or recoText.lower().find("dis-moi la date") !=-1 or recoText.lower().find("donne-moi la date") != -1:
                now = datetime.datetime.now()
                tts("nous sommes le "+now.strftime("%A")+" "+str(now.strftime("%d"))+" "+now.strftime("%B")+" "+str(now.strftime("%Y")))
            #Get weather
            elif recoText.lower().find("quel temps fait-il") !=-1 or recoText.lower().find("quelle est la météo") !=-1:
                sayWeather()
            #Get news from rss
            elif recoText.lower().find("quoi de neuf") !=-1:
               readNews()
            elif recoText.lower().find("test réveil") !=-1:
               wakeUp()
            elif recoText.lower().find("minuteur") !=-1:
                #set default value to raise an exeption if not set after (not planned)
                posMin = -1
                posHr = -1
                posSec = -1
                hr = -1
                min = -1
                sec = -1
                findTimingStr = recoText.lower().split()

                if findTimingStr.count("heures") + findTimingStr.count("heure") > 1 or findTimingStr.count("minutes") + findTimingStr.count("minute") > 1 or findTimingStr.count("seconde") + findTimingStr.count("secondes") > 1:
                    tts("Pardon je n'ai pas compris la durée de votre minuteur")
                #get number of hours
                if findTimingStr.count("heures") == 1:
                    posHr = findTimingStr.index("heures")
                    if findTimingStr[posHr - 1].isdigit() :
                        hr = findTimingStr[posHr - 1]
                    else:
                        if findTimingStr[posHr - 1] == "une":
                            hr = 1
                    
                if findTimingStr.count("heure") == 1:
                    posHr = findTimingStr.index("heure")
                    if findTimingStr[posHr - 1].isdigit() :
                        hr = findTimingStr[posHr - 1]
                    else:
                        if findTimingStr[posHr - 1] == "une":
                            hr = 1
                #get number of minutes
                if findTimingStr.count("minutes") == 1:
                    posMin = findTimingStr.index("minutes")
                    if findTimingStr[posMin - 1].isdigit() :
                        min = findTimingStr[posMin - 1]
                    else:
                        if findTimingStr[posMin - 1] == "une":
                            min = 1
                
                if findTimingStr.count("minute") == 1:
                    posMin = findTimingStr.index("minute")
                    if findTimingStr[posMin - 1].isdigit() :
                        min = findTimingStr[posMin - 1]
                    else:
                        if findTimingStr[posMin - 1] == "une":
                            min = 1
                #get number of secondes
                if findTimingStr.count("secondes") == 1 :
                    posSec = findTimingStr.index("secondes")
                    if findTimingStr[posSec - 1].isdigit() :
                        sec = findTimingStr[posSec - 1]
                    else:
                        if findTimingStr[posSec - 1] == "une":
                            sec = 1

                if findTimingStr.count("seconde") == 1:
                    posSec = findTimingStr.index("seconde")
                    if findTimingStr[posSec - 1].isdigit() :
                        sec = findTimingStr[posSec - 1]
                    else:
                        if findTimingStr[posSec - 1] == "une":
                            sec = 1
                #get number of seconde even if the "secondes" is not present
                if posMin != -1 and posSec == -1 and posMin < len(findTimingStr) -1:
                    if findTimingStr[posMin + 1].isdigit() :
                        sec = findTimingStr[posMin + 1]
                
                #set all default value for easier work with the class
                if hr == -1:
                    hr = 0
                if min == -1:
                    min = 0
                if sec == -1:
                    sec = 0
                # 1 hour or more
                if int(hr) >= 1 and int(min) >= 1 and int(sec) >= 1:
                    tts("un minuteur de " + str(hr) + " heures " + str(min) + " minutes et " + str(sec) + "secondes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                elif int(hr) >= 1 and int(min) >= 1 and int(sec) < 1:
                    tts("un minuteur de " + str(hr) + " heures " + str(min) + " minutes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                elif int(hr) >= 1 and int(min) < 1 and int(sec) >= 1:
                    tts("un minuteur de " + str(hr) + " heures et " + str(sec) + "secondes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                # 1 minute or more
                elif int(hr) < 1 and int(min) >= 1 and int(sec) < 1:
                    tts("un minuteur de " + str(min) + " minutes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                elif int(hr) < 1 and int(min) >= 1 and int(sec) >= 1:
                    tts("un minuteur de " + str(min) + " minutes et " + str(sec) + "secondes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                # 1 seconde or more
                elif int(hr) < 1 and int(min) < 1 and int(sec) >= 1:
                    tts("un minuteur de " + str(sec) + "secondes a été lancé" )
                    timer = minuteur(hr,min,sec)
                    timer.start()
                else:
                    #if all unit are set to 0
                    tts("un minuteur doit avoir une duré d'au moins 1 seconde")

            #Message par defaut  /incomprehention       
            else:
                tts("Pardon je n'ai pas compris.")

            nbClap = 0